# EKS requires the usage of Virtual Private Cloud to provide the base for its networking configuration.
# The below will create a 69.0.0.0/16 VPC, two 69.0.X.0/24 subnets, an internet gateway, and setup the subnet routing to route external traffic through the internet gateway:
provider "aws" {
  region = var.region
}
terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/18766292/terraform/state/tf_eks"
    lock_address = "https://gitlab.com/api/v4/projects/18766292/terraform/state/tf_eks/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/18766292/terraform/state/tf_eks/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
}
module "network" {
  source = "./modules/network"

  // pass variables from .tfvars
  region = var.region
  user = var.user
  vpc_name = var.vpc_name
  //  cluster_name = var.cluster_name
  subnet_count = 2
}

module "security_groups" {
  source = "./modules/security_groups"

  // pass variables from .tfvars
  accessing_computer_ip = var.accessing_computer_ip
  cluster_name = var.cluster_name
  region = var.region
  // inputs from modules
  vpc_id = module.network.vpc_id
}

module "eks" {
  source = "./modules/eks"

  // pass variables from .tfvars
  cluster_name = var.cluster_name
  vpc_id = module.network.vpc_id
  region = var.region
  accessing_computer_ip = var.accessing_computer_ip
  app_subnet_ids = module.network.app_subnet_ids
}
