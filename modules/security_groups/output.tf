
output "sg_master" {
  description = "Cluster Master SG"
  value = aws_security_group.eks_master_sg.id
}

output "sg_node" {
  description = "Cluster Node SG"
  value = aws_security_group.eks_node_sg.id
}