package test

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	ttaws "github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/magiconair/properties/assert"
	"math/rand"
	"net"
	"os"
	"testing"
	"time"
	"log"
)

// Get preferred outbound ip of this machine
func getOutboundIP() net.IP {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}


func TestIntSecurityGroup(t *testing.T) {
	t.Parallel()
	/*
	 * Create VPC
	 */
	//retryDescription := "Return value after 5 retries with expected error, match regex"
	//retryableErrors := map[string]string{
	//	".*VpcLimitExceeded.*": "Error creating VPC: VpcLimitExceeded: The maximum number of VPCs has been reached. Try another region.",
	//}
	//maxRetries := 5
	//actualOutput, err := retry.DoWithRetryableErrorsE(t, retryDescription, retryableErrors, maxRetries, 1*time.Millisecond, func() (string, error) {
	awsRegion := ttaws.GetRandomStableRegion(t, nil, nil)
	os.Setenv("AWS_DEFAULT_REGION", awsRegion)
	rand.Seed(time.Now().UnixNano())
	// For Testing, we are going to just use the default VPC
	awsVPC := ttaws.GetDefaultVpc(t, awsRegion)
	//vpcId := terraform.Output(t, terraformOptions, "vpc_id")
	//r := rand.New(time.Now().UnixNano())
	rand.Seed(time.Now().UnixNano())

	randVal := rand.Intn(99)
	testClusterName := fmt.Sprint("test", randVal)
	testIP := getOutboundIP()
	terraformOptions := &terraform.Options{
		TerraformDir: "../modules/security_groups",

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			"cluster_name": testClusterName,
			"accessing_computer_ip": testIP.String(),
			"vpc_id": awsVPC.Id,
			"region": awsRegion,
		},
	}

	defer terraform.Destroy(t, terraformOptions)

	terraform.InitAndApply(t, terraformOptions)
	sg_master_id := terraform.Output(t, terraformOptions, "sg_master")

	// Get info of the SG from AWS
	svc := ec2.New(session.New(), & aws.Config{
		Region: aws.String(awsRegion),
	})
	masterInput := &ec2.DescribeSecurityGroupsInput{
		GroupIds: []*string{
			aws.String(sg_master_id),
		},
	}

	masterResult, err := svc.DescribeSecurityGroups(masterInput)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return
	}

	masterSG := (*masterResult).SecurityGroups[0]

	assert.Equal(t, awsVPC.Id, *masterSG.VpcId)
	// We should definately do more here but I need to move on.

	/**********************************************************/

	SgNodeId := terraform.Output(t, terraformOptions, "sg_node")

	nodeInput := &ec2.DescribeSecurityGroupsInput{
		GroupIds: []*string{
			aws.String(SgNodeId),
		},
	}

	nodeResult, err := svc.DescribeSecurityGroups(nodeInput)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return
	}

	nodeSG := *nodeResult.SecurityGroups[0]
	assert.Equal(t, awsVPC.Id, *nodeSG.VpcId)
	// We should definately do more here but I need to move on.

}
